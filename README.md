# Smart Contract Project: JOGA Token and PersonalSmartWallet

## Project Overview

This project showcases the implementation of two Ethereum smart contracts using Solidity: JOGA Token and PersonalSmartWallet. The JOGA Token contract is an ERC20 token that facilitates token minting and ownership management. The PersonalSmartWallet contract provides secure management of ERC20 tokens and Ether transfers, allowing authorized users to withdraw tokens to the owner and transfer tokens and Ether to specified addresses.

## Project Smart Contracts

### JOGA Token Contract

- **Description**: JOGA Token is an ERC20 token named JOGAToken (symbol: JOG).
- **Functionality**: It supports minting of tokens to designated addresses and includes basic ownership controls.
- **Use Case**: Suitable for applications requiring a custom ERC20 token with minting capabilities.
- **Blockchain Explorer**: [visit](https://sepolia.etherscan.io/address/0x587354d8AABA236Efe8B965592914d1f00d51202).

### PersonalSmartWallet Contract

- **Description**: PersonalSmartWallet manages ERC20 tokens and Ether with authorization controls.
- **Functionality**: Allows the owner to withdraw ERC20 tokens to their address and authorized users to transfer tokens and Ether securely.
- **Use Case**: Ideal for applications needing secure and controlled management of token and Ether transfers within a smart contract.
- **Blockchain Explorer**: [visit](https://sepolia.etherscan.io/address/0xc2e9a7D6bF0E24C1351227Df02Cb7FCc73eDB578).

## Acknowledgements

- Solidity
- Ethereum
- Hardhat
- OpenZeppelin Contracts
- ethers.js
- Infura
- Sepolia Testnet