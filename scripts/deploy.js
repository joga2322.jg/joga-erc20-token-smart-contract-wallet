const {ethers} = require('hardhat')

async function deploy() {
  // const tokenFactory = await ethers.getContractFactory('JOGA')
  // const JOGATokenContract = await tokenFactory.deploy()
  // const JOGATokenAdress = await JOGATokenContract.getAddress()
  // console.log('Token address: ', JOGATokenAdress)

  const walletFactory = await ethers.getContractFactory('PersonalSmartWallet')
  const smartWallet = await walletFactory.deploy()
  const smartWalletAddress = await smartWallet.getAddress()
  console.log('Wallet address: ', smartWalletAddress)
}

deploy()
  .then(() => process.exit(0))
  .catch((err) => {
    console.error(err.message)
    process.exit(1)
  })