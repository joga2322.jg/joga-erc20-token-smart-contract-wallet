const { ethers } = require("hardhat");
const assert = require("assert");

describe("token & personal wallet test", () => {
  let accounts;
  let token;
  let wallet;
  let tokenAddress;
  let walletAddress;

  beforeEach(async () => {
    accounts = await ethers.getSigners();
    const tokenFactory = await ethers.getContractFactory("JOGA", accounts[0]);
    token = await tokenFactory.deploy();
    tokenAddress = await token.getAddress();

    const walletFactory = await ethers.getContractFactory(
      "PersonalSmartWallet",
      accounts[0]
    );
    wallet = await walletFactory.deploy();
    walletAddress = await wallet.getAddress();
  });

  it("could deploy contracts properly", () => {
    assert.ok(tokenAddress);
    assert.ok(walletAddress);
  });

  it("could mint token, send to wallet, withdraw, & transfer token/ETH from smart contract wallet", async () => {
    await token
      .connect(accounts[0])
      .mint(accounts[0], ethers.parseEther("4000"));

    let accountBalance = ethers.formatEther(await token.balanceOf(accounts[0]));
    assert.equal(accountBalance, "4000.0");

    await token
      .connect(accounts[0])
      .transfer(walletAddress, ethers.parseEther("125"));

    let walletTokenBalance = ethers.formatEther(
      await wallet.checkERC20TokenBalance(tokenAddress)
    );
    assert.equal(walletTokenBalance, "125.0");

    await wallet.addAuthorizedAddress(accounts[1]);
    await wallet
      .connect(accounts[1])
      .transferERC20Token(
        tokenAddress,
        ethers.parseEther("50"),
        accounts[2].address
      );

    assert.equal(
      ethers.formatEther(await token.balanceOf(accounts[2])),
      "50.0"
    );
    assert.equal(
      ethers.formatEther(await wallet.checkERC20TokenBalance(token)),
      "75.0"
    );

    await accounts[0].sendTransaction({
      to: walletAddress,
      value: ethers.parseEther("10"),
    });

    let walletETHBalance = ethers.formatEther(await wallet.checkETHBalance());
    assert.equal(walletETHBalance, "10.0");

    await wallet
      .connect(accounts[1])
      .transferETH(accounts[2].address, ethers.parseEther("8"));
    assert.equal(
      ethers.formatEther(await ethers.provider.getBalance(accounts[2])),
      "10008.0"
    );
  });
});
