/** @type import('hardhat/config').HardhatUserConfig */
require('dotenv').config({ path: '.env.local' })
require('@nomicfoundation/hardhat-toolbox')

const etherscanKey = process.env.ETHERSCAN_API_KEY ?? ''
const sepoliaUrl = process.env.SEPOLIA_API_KEY ?? ''
const accountKey = process.env.PRIVATE_KEY ?? ''

module.exports = {
  solidity: "0.8.20",
  networks: {
    sepolia: {
      url: sepoliaUrl,
      accounts: [accountKey]
    },
    localnet: {
      url: "http://127.0.0.1:8545/"
    }
  },
  etherscan: {
    apiKey: etherscanKey
  }
};
