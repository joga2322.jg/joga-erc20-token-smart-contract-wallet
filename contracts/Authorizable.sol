// SPDX-License-Identifier: SEE LICENSE IN LICENSE
pragma solidity 0.8.20;

import "@openzeppelin/contracts/access/Ownable.sol";

contract Authorizable is Ownable{
  mapping(address => bool) public authorizedAddresses;
    
  modifier onlyAuthorized {
    require(authorizedAddresses[msg.sender] || owner() == msg.sender, "Unauthorized access");
    _;
  }

  function addAuthorizedAddress(address newAddress) public onlyOwner {
    require(newAddress != address(0));
    authorizedAddresses[newAddress] = true;
  }

  function removeAuthorizedAddress(address removedAddress) public onlyOwner {
    require(removedAddress != address(0));
    require(removedAddress != msg.sender);
    authorizedAddresses[removedAddress] = false;
  }
}