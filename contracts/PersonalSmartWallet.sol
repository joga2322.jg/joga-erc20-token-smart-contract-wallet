// SPDX-License-Identifier: MIT
pragma solidity 0.8.20;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./Authorizable.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";

contract PersonalSmartWallet is Authorizable, ReentrancyGuard  {
  modifier recieverValidation(address receiver) {
    require(receiver != address(0), 'Receiver address invalid');
    _;
  }

  function withdrawERC20TokenToOwner(address token, uint256 amount) external onlyOwner {
    IERC20(token).transfer(owner(), amount);
  }

  function transferERC20Token(address token, uint256 amount, address receiver) external onlyAuthorized recieverValidation(receiver){
    IERC20(token).transfer(receiver, amount);
  }

  function transferETH(address receiver, uint256 amount) external onlyAuthorized recieverValidation(receiver) {
    (bool sent,) = receiver.call{value: amount}("");
    require(sent, "Failed to send Ether");
  }

  function checkERC20TokenBalance(address token) public view returns(uint256) {
    return IERC20(token).balanceOf(address(this));
  }

  function checkETHBalance() public view returns(uint256) {
    return address(this).balance;
  }

  receive() external payable {}
}